
%%
\RequirePackage{ifluatex}
\ifluatex
\else
	\RequirePackage{pdf14}
\fi

\documentclass[aspectratio=0403,xcolor={table,dvipsnames}]{beamer}
%%
%% E.ON ERC Beamer Theme (layout based on E.ON ERC PowerPoint template)
%% by Simon Pickartz, Institute for Automation of Complex Power Systems, 2014
%%   spickartz@eonerc.rwth-aachen.de
%% based on RWTH Latex template
%% by Georg Wassen, Lehrstuhl für Betriebssysteme, 2013
%%    wassen@lfbs.rwth-aachen.de
%%
%% The templates are derived from the beamer documentation and the provided
%% templates, hence, the same licence applies:
%%
%% ----------------------------------------------------------------
%% |  This file may be distributed and/or modified                |
%% |                                                              |
%% |  1. under the LaTeX Project Public License and/or            |
%% |  2. under the GNU Public License.                            |
%% |                                                              |
%% |  See the file doc/licenses/LICENSE for more details.         |
%% ----------------------------------------------------------------
%%
%% Version 0.1    03/28/2014    Initial presentation using this theme
%% Version 0.2    04/01/2014    First version to be published in the institute
%%


\usepackage[]{luainputenc}
\usepackage[english]{babel}
\usetheme{eonerc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{graphicx}
\beamertemplatenavigationsymbolsempty
\usepackage{tikz}
\usetikzlibrary{shadings}
\usetikzlibrary{calc}
\usepackage{textcomp}

% For tables
%\RequirePackage[usenames, dvipsnames]{color}
\usepackage[table]{xcolor}
\usepackage{booktabs}
\newcommand{\failed}{\cellcolor{red}FAILED}
\newcommand{\passed}{\cellcolor{green}PASSED}



\title[Modernization of jabber.rwth-aachen.de]{Modernization of the central XMPP/\\Jabber-Server at RWTH Aachen University}
\titlebanner{pictures/title_small}
\subtitle{Presentation of Bachelor Thesis}
\author{Philipp Hübner}
\institute[ACS]{Automation of Complex Power Systems}
\webaddress{www.acs.eonerc.rwth-aachen.de}
\date{04/13/2017}
\subject{Modernization of the central XMPP/Jabber-Server at RWTH Aachen University}
\keywords{Jabber, XMPP, Django, Ansible}
\addlogo{logos/xmpp_logo.png}


\begin{document}

\begin{frame}{Agenda}
	\tableofcontents
\end{frame}


\section{XMPP/Jabber in general}

\subsection{Introduction}
\begin{frame}{Introduction}
	\begin{block}{XMPP: eXtensible Messaging and Presence Protocol}
		\pause
		\begin{itemize}
			\itemsep=5pt
			\item Message exchange based on XML and TCP/IP
			\item Near real-time communication
			\item Presence $\Leftrightarrow$ status information
			\item Protocol $\Leftrightarrow$ set of rules for digital communication
			\pause
			\item XMPP originally called ''Jabber''
			\item Standardized by IETF in RFCs 6120, 6121, 7395, 7590, and 7622
			\item XMPP Extension Protocols (XEPs) standardized by XMPP Standards Foundation (XSF)
		\end{itemize}
	\end{block}
\end{frame}

\subsection{XMPP basics}
\begin{frame}{XMPP basics 1/2}
	\begin{block}{Jabber Identifiers (JIDs)}
		\pause
		\begin{itemize}
			\itemsep=5pt
			\item Unique, like a telephone numbers
			\item Structure: <user>\,@\,<domain>\,/\,<resource>
			\begin{itemize}
				\item[\textrightarrow] Distributed and federated XMPP network
			\end{itemize}
		\end{itemize}
		\centering
		\includegraphics[height=0.65\textheight]{../drawings/federated-network}
	\end{block}
\end{frame}
\begin{frame}{XMPP basics 2/2}
	\begin{block}{XMPP clients}
		\begin{itemize}
			\itemsep=5pt
			\item Connect to XMPP servers
			\item Enable users to participate in the XMPP network
			\item Example: Gajim
		\end{itemize}
		\centering
		\includegraphics[height=0.6\textheight]{pictures/gajim_roster}
		%\includegraphics[height=0.5\textheight]{pictures/gajim_tabbed_chat}
		\includegraphics[height=0.6\textheight]{pictures/groupchat_window}
	\end{block}
\end{frame}

\subsection{XMPP usage today}
\begin{frame}{XMPP usage today 1/3}
	\pause
	\begin{itemize}
		\itemsep=1em
		\item Instant Messaging (IM)
		\item Social Web
		\item Real-time communication (RTC)
		\item Internet of Things (IoT)
	\end{itemize}
\end{frame}

\begin{frame}{XMPP usage today 2/3}
	\begin{columns}[c]
		\begin{column}{.45\textwidth}
			\centering
			OpenADR 2.0\\
			{\tiny Standard for Automated Demand Response}
		\end{column}
		\begin{column}{.55\textwidth}
			\centering
			\includegraphics[width=\textwidth]{../drawings/openadr}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{XMPP usage today 3/3}
	\begin{columns}[c]
		\begin{column}{.45\textwidth}
			\centering
			IEC 61850-8-2\\
			\vspace{6pt}
			{\tiny Standard for power utility automation\\
			Part 8-2: Specific Communication Service Mapping (SCSM) -- Mapping to Extensible Messaging Presence Protocol
			(XMPP)}
		\end{column}
		\begin{column}{.55\textwidth}
			\centering
			\includegraphics[height=1\textheight]{../drawings/iec61850}
		\end{column}
	\end{columns}
\end{frame}


\section{XMPP/Jabber at RWTH Aachen University}

\subsection{Usage}
\begin{frame}{Usage at RWTH Aachen University}
	\pause
	\begin{block}{Users}
		\begin{itemize}
			\item Students
			\item Staff
			\item Anybody with an e-mail address from RWTH Aachen University
		\end{itemize}
	\end{block}
	\begin{block}{Types of use}
		\begin{itemize}
			\item Personal IM
			\item Multi User Chatrooms (MUCs)
			\item Shared Roster Groups
		\end{itemize}
	\end{block}
\end{frame}

\subsection{History}
\begin{frame}{History 1/3}
	\pause
	\begin{itemize}
		\item[]
		\begin{itemize}
			\itemsep=5pt
			%\item[1988:] Internet Relay Chat (IRC)
			%\item[1993:] RFC 1459: Internet Relay Chat Protocol
			%\item[1996:] ICQ
			%\item[1997:] AOL Instant Messenger
			%\item[1998:] AOL acquires ICQ for \textasciitilde\$300 million
			\item[1999:] Jeremie Miller announces the existence of Jabber, a community emerges
			\item[2001:] Jabber Software Foundation (JSF) is formed, Jabber Extension Protocols (JEPs) are being developed
			\item[2004:] RFCs 3920, 3921: Extensible Messaging and Presence Protocol (XMPP)
			\item[2005:] Google Talk, based on XMPP, without federation, open
			\item[2006:] Google Talk allows federation, GMX and Web.de join in
			\item[2006:] Sven Burmeister (RWTH student) installs XMPP server at\\
				     Halifax dormitory: \textbf{\emph{jabber.rwth-aachen.de}} is born!
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{History 2/3}
	\begin{itemize}
		\item[]
		\begin{itemize}
			\itemsep=5pt
			\item[2007:] Jabber Software Foundation renames itself to XMPP Standards Foundation, JEPs become XEPs
			\item[2007:] \emph{jabber.rwth-aachen.de} now hosted at IT Center
			\item[2008:] Facebook Chat, based on XMPP, without federation, open
			\item[2009:] WhatsApp, based on XMPP, without federation, closed
			\item[2011:] RFC 6120, 6121: updated core definition of XMPP
			\item[2012:] Maintenance of \emph{jabber.rwth-aachen.de} down to minimum
			%\item[2013:] Google removes XMPP support from Talk client
			%\item[2014:] Facebook buys WhatsApp for \textasciitilde\$19 billion and deprecates its XMPP chat
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{History 3/3}
	\begin{itemize}
		\item[]
		\begin{itemize}
			\itemsep=5pt
			\item[2014:] RFC 7395: XMPP over WebSocket
			\item[2014:] \emph{jabber.rwth-aachen.de} becomes unmaintained
			%\item[2015:] Facebook removes XMPP support
			\item[2015:] RFC 7590: Use of Transport Layer Security (TLS) in XMPP
			\item[2015:] RFC 7622: modernized definition of XMPP addresses
			\item[2016:] \emph{jabber.rwth-aachen.de} completely handed over to the IT Center
			%\item[2016:] Philipp Hübner (RWTH student) modernizes \emph{jabber.rwth-aachen.de} for his Bachelor Thesis
			%\item[2017:] XSF accepts a Special Interest Group on IoT
			%\item[2017:] Management of shared roster groups added to homepage of \emph{jabber.rwth-aachen.de}
			%\item[2017:] Google Talk servers shut down completely
		\end{itemize}
	\end{itemize}
\end{frame}

\subsection{Problems}
\begin{frame}{Problems 1/2}
	\pause
	\centering
	\includegraphics[height=0.95\textheight]{../screenshots/xmppnet_old}
\end{frame}

\begin{frame}{Problems 2/2}
	\begin{itemize}
		\itemsep=5pt
		\item Outdated software
		\begin{itemize}
			\item[\Rightarrow] No support for mobile devices
			\item[\Rightarrow] Impaired security
			\item[\Rightarrow] Impaired connectivity with other XMPP servers
			\item[\Rightarrow] Impaired usability
		\end{itemize}
		\pause
		\item Outdated website/wiki content
		\begin{itemize}
			\item[\Rightarrow] Incorrect information towards users
			\item[\Rightarrow] Only in German
		\end{itemize}
		\pause
		\item Helga bot not available anymore
		\begin{itemize}
			\item[\Rightarrow] Users could no longer manage shared roster groups
		\end{itemize}
		\pause
		\item No technical documentation
		\begin{itemize}
			\item[\Rightarrow] System unmaintainable
		\end{itemize}
	\end{itemize}
\end{frame}

\subsection{Solutions}
\begin{frame}{Solutions 1/2 - Roadmap}
	\pause
	\begin{enumerate}
		\itemsep=5pt
		\item Quick wins
		\begin{itemize}
			\item Correct SSL certificate
			\item Newer Java version
		\end{itemize}
		\pause
		\item Full replacement
		\begin{itemize}
			\item OS and XMPP server software \textrightarrow{} Debian GNU/Linux + ejabberd
			\item Deployment \textrightarrow{} Ansible
			\item Migrating the user base \textrightarrow{} custom Python scripts
			\item Creating a new website \textrightarrow{} Django
			\item Collecting and processing statistics \textrightarrow{} Munin
		\end{itemize}
		\pause
		\item Managing shared roster groups \textrightarrow{} custom homepage addon
	\end{enumerate}
\end{frame}

\begin{frame}{Solutions 2/2 - System overview}
	\centering
	\includegraphics[height=0.95\textheight]{../drawings/system}
\end{frame}

\subsection{Results}
\begin{frame}{Results 1/3 - New homepage}
	\pause
	\includegraphics[width=1.035\textwidth]{pictures/hp_new}
\end{frame}

\begin{frame}{Results 2/3 - Compliance test results}
	\pause
	\begin{table}[htbp]
		\resizebox{1.3\textheight}{!}{%
		\centering
		\begin{tabular}{lcccc}
			\midrule
			\addlinespace XMPP Server                             & & Openfire 3.9.1      & & ejabberd 16.09      \\
			\addlinespace Date of test                            & & October 1st 2016    & & April 1st 2017      \\
			\midrule
			\addlinespace XEP-0045: Multi-User Chat               & & \passed             & & \passed             \\
			\addlinespace XEP-0065: SOCKS5 Bytestreams (Proxy)    & & \passed             & & \passed             \\
			\addlinespace XEP-0115: Entity Capabilities           & & \failed             & & \passed             \\
			\addlinespace XEP-0163: Personal Eventing Protocol    & & \passed             & & \passed             \\
			\addlinespace XEP-0191: Blocking Command              & & \failed             & & \passed             \\
			\addlinespace XEP-0198: Stream Management             & & \failed             & & \passed             \\
			\addlinespace XEP-0237: Roster Versioning             & & \failed             & & \passed             \\
			\addlinespace XEP-0280: Message Carbons               & & \failed             & & \passed             \\
			\addlinespace XEP-0313: Message Archive Management    & & \failed             & & \passed             \\
			\addlinespace XEP-0352: Client State Indication       & & \failed             & & \passed             \\
			\addlinespace XEP-0357: Push Notifications            & & \failed             & & \passed             \\
			\addlinespace XEP-0363: HTTP File Upload              & & \failed             & & \passed             \\
			\addlinespace XEP-0368: SRV records for XMPP over TLS & & \failed             & & \passed             \\
			\addlinespace
			\midrule
		\end{tabular}}
	\end{table}
\end{frame}

\begin{frame}{Results 3/3 - Security test results}
	\pause
	\centering
	\includegraphics[height=0.9\textheight]{../screenshots/xmppnet_new}
\end{frame}


\section{Live demonstration}
\begin{frame}{Live demonstration}
	The choice is yours:
	\begin{enumerate}
	\item How to use a Jabber client
	\item The new homepage of jabber.rwth-aachen.de
	\item Managing shared roster groups
	\item Running the XMPP compliance tester
	\item Current statistics in Munin
	\item Ansible in action
	\end{enumerate}
	\vspace{1cm}
	Questions?
\end{frame}


\lastpage

\input{appendix.tex}

\end{document}
