\chapter{Ansible}
\label{apx:ansible}
This appendix describes how Ansible was used to deploy the new XMPP/Jabber server.

\blockcquote{docs.ansible.com}{Ansible is an IT automation tool.
It can configure systems, deploy software,
and orchestrate more advanced IT tasks such as continuous deployments or zero downtime rolling updates.
Ansible’s main goals are simplicity and ease-of-use.
It also has a strong focus on security and reliability, featuring a minimum of moving parts,
usage of OpenSSH for transport (with an accelerated socket mode and pull modes as alternatives),
and a language that is designed around auditability by humans – even those not familiar with the program.}

To automate a process with Ansible,
one writes an Ansible \emph{playbook}, which is a simple text file in YAML syntax containing all information Ansible needs to know:
tasks, variables, defaults, handlers and more.
Every action that Ansible should take must be defined in a task or a handler.
Handlers are more or less the same as tasks,
but only executed after specific events in order to \emph{handle} them appropriately.
A task consists of at least a name and a module.
The name can be chosen freely, but should be chosen as descriptive and helpful as possible.
If no name is specified,
Ansible automatically derives one from the task at hand.
The module must be one of those included with the Ansible release,
a complete list is available at \mbox{\url{http://docs.ansible.com/ansible/modules}}.
Here is a simple example:
\begin{verbatim}
---
- hosts: jabber
  tasks:
   - name: Install postfix
     apt: name=postfix state=latest
   - name: Copy /etc/gai.conf to prefer IPv4 until IPv6 becomes
           available
     copy: src=files/gai.conf dest=/etc/gai.conf
           owner=root group=root mode=0644
---
\end{verbatim}
The first task uses the module \emph{apt} to install the package \emph{postfix},
the second task copies the local file \emph{files/gai.conf} to the target system as \emph{/etc/gai.conf}
and sets the configured owner, group and permissions.
This example barely scratches the surface of what Ansible can do,
for more information see \url{https://docs.ansible.com/ansible/}.


\section*{Ansible playbook for jabber.rwth-aachen.de}
A playbook as described above was created to set up jabber.rwth-aachen.de.
This playbook can also be used to verify the setup and to deploy updates.
Figure~\ref{fig:ansible} shows what the output of a successful Ansible verification run looks like.
The complete playbook is almost 500 lines long and has been published at
github.com\footnote{\url{https://github.com/debalance/ansible-playbook-jabber.rwth-aachen.de}}
in the hope that it may be useful to others.
Obviously,
all sensitive data like passwords,
keys and certificates has been removed beforehand.

\begin{figure}[h]
\ttfamily
\scriptsize
\rule{1\textwidth}{0.3pt}\\
\ldots\\
\ldots\\
TASK [Install munin] ***********************************************************\\
{\color{Green}
ok: [jabber-dev.itc.rwth-aachen.de] => (item=[u'munin', u'munin-node', u'munin-plugins-c', u'munin-plugins-core'])\\
}\\
TASK [Copy munin config] *******************************************************\\
{\color{Green}
ok: [jabber-dev.itc.rwth-aachen.de]\\
}\\
TASK [Copy munin-node config] **************************************************\\
{\color{Green}
ok: [jabber-dev.itc.rwth-aachen.de]\\
}\\
TASK [Copy some custom munin plugins from https://github.com/fsinf/munin] ******\\
{\color{Green}
ok: [jabber-dev.itc.rwth-aachen.de] => (item=df)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=df\_abs)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=ejabberd)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=ejabberd\_active\_users\_7)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=ejabberd\_active\_users\_30)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=ejabberd\_c2s\_types)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=ejabberd\_registered)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=ejabberd\_uptime)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=muc\_count)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=rocheck)\\
}\\
TASK [Disable some plugins that are not needed or not working] *****************\\
{\color{Green}
ok: [jabber-dev.itc.rwth-aachen.de] => (item=/etc/munin/plugins/nfs4\_client)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=/etc/munin/plugins/nfsd4)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=/etc/munin/plugins/nfs\_client)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=/etc/munin/plugins/nfsd)\\
}\\
TASK [Disable munin's default apache config] ***********************************\\
{\color{Green}
ok: [jabber-dev.itc.rwth-aachen.de]\\
}\\
TASK [Make sure services are running] ******************************************\\
{\color{Green}
ok: [jabber-dev.itc.rwth-aachen.de] => (item=apache2)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=celery)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=cron)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=ejabberd)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=munin-node)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=mysql)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=postfix)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=redis-server)\\
ok: [jabber-dev.itc.rwth-aachen.de] => (item=uwsgi)\\
}\\
PLAY RECAP *********************************************************************\\
{\color{Green}
jabber-dev.itc.rwth-aachen.de : ok=94   changed=0    unreachable=0    failed=0\\
}\\
\rule{1\textwidth}{0.3pt}
\caption[Ansible screenshot]{Screenshot of a successful Ansible verification run}
\label{fig:ansible}
\end{figure}
