MAINTEX 	:= thesis.tex

LATEX 		:= lualatex
FLAGS 		:= -quiet -shell-escape

.PHONY : default help pdf verbose clean veryclean 

default :
	pdflatex $(MAINTEX)
	makeindex -s thesis.ist -o thesis.gls thesis.glo
	latexmk -$(LATEX) $(FLAGS) $(MAINTEX)

help :
	@echo ""
	@echo "This Makefile creates the PDF of the thesis by using 'latexmk'"
	@echo "  make             : Generate PDF of the thesis"
	@echo "  make pdf         : Generate PDF of the thesis (forced mode)"
	@echo "  make verbose     : Show output from latex compiler"
	@echo "  make clean       : Delete temporary files"
	@echo "  make veryclean   : Delete temporary files including PDF"
	@echo ""


pdf : default

verbose :
	pdflatex $(MAINTEX)
	makeindex -s thesis.ist -o thesis.gls thesis.glo
	latexmk -g -$(LATEX) $(FLAGS) -verbose $(MAINTEX)

clean :
	latexmk -c
	rm -f *.lol chapters/*.aux *.fls thesis-blx.bib *.xml *.bbl svg/*.pdf thesis.glo thesis.gls thesis.ist

veryclean : clean
	latexmk -C
