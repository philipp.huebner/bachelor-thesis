\chapter{Implementation}
\label{chap:implementation}

Before implementing a new system it was sensible to analyze the problems of the current one
to see if there were any quick wins available to reduce the amount of technical problems the users were facing while the new system was being prepared.
As it turned out there were several.

\paragraph{SSL certificate:}
When an \gls{XMPP} server opens an outgoing \gls{s2s} connection to another \gls{XMPP} server,
the server opening the connection becomes a client and for that reason must have the flag \emph{TLS Web Client Authentication}
in its server certificate\footnote{The field for such flags in the certificate is called \emph{X509v3 Extended Key Usage}.}
as well as the flag \emph{TLS Web Server Authentication}.
The \gls{SSL} certificate installed on jabber.rwth-aachen.de was missing the client flag,
it only had the server flag set.
As a result,
many remote servers refused the incoming connection with a message such as
\emph{Won't accept certificate of jabber.rwth-aachen.de: unsupported certificate purpose}.
Once the IT Center's \gls{CA} had issued a new and correct certificate and that was imported into the \gls{XMPP} service,
communication with many remote servers became instantly possible again.

\paragraph{Java version:}
Openfire is written in Java and the Openfire version (3.9.1) that was installed on jabber.rwth-aachen.de ships with Java 1.7 by default.
However,
that Java bundle was removed and the Java installation of the \gls{OS} was used instead,
since that would automatically receive security updates.
Unfortunately,
the \gls{OS} had only Java 1.6 installed,
which is significantly worse in supporting \gls{TLS} than Java 1.7.
For example,
Java 1.6 does not support \gls{TLS} 1.2 yet.
By replacing Java 1.6 with Java 1.7,
the key exchange score for jabber.rwth-aachen.de on xmpp.net went up from 40 to 80
and the cipher score went up from 50 to 60,
resulting in an overall grade of 'B' instead of the previous
'C'\footnote{These scores and grades will be discussed and explained in detail in \cref{chap:evaluation}.}.
Through this change it became instantly possible to communicate with many servers that required more secure \gls{TLS} connections and therefore insisted on only using \gls{TLS} 1.2.

\paragraph{IPv4 vs. IPv6:}
For technical reasons which are outside the scope of this thesis,
the IT Center could not provide jabber.rwth-aachen.de with IPv6 network connectivity yet.
Going through the logfiles of Openfire a conflict with IPv4 versus IPv6 became apparent,
causing connection attempts to other servers to fail:
Although jabber.rwth-aachen.de only had IPv4 network connectivity with the outside world,
the \gls{DNS} servers of the RWTH Aachen University returned both A and AAAA
records\footnote{A records are for IPv4 addresses, AAAA records for IPv6 addresses.}
for lookup requests from Openfire.
Openfire preferred IPv6 and tried to connect to those addresses first.
Only when that connection attempt had failed multiple times
the corresponding IPv4 address was used for a connection attempt.
By restarting Openfire with the setting \emph{java.net.preferIPv4Stack=true} that behavior could be changed.
As a result,
outgoing connections were established a lot faster and in some rare cases started working for the first time after this change.

\paragraph{Openfire version:}
Openfire 3.9.3 compared to the installed version 3.9.1 had about 60 bugfixes,
15 improvements,
and 10 new features\footnote{\url{http://download.igniterealtime.org/openfire/docs/3.9.3/changelog.html}}.
At the same time,
a difference in the third digit of the version number indicates only minor changes that should not break anything.
Weighing the seemingly small risk of breakage against the long list of small improvements,
the decision was made to implement the upgrade.
The upgrade did not interfere with anything and the users benefited from a slightly better working \gls{XMPP} server,
but there were no noticeable changes in regards to connectivity with other servers.
The scores on xmpp.net remained unchanged as well.


\section{Choosing an OS and XMPP server software}
Now that the old system was in an acceptable state for the time being,
a complete replacement was to be implemented.
The first thing to do was choosing an \gls{OS} and an \gls{XMPP} server.

When looking through the test results and the public server directory on xmpp.net
it becomes apparent that there are only two major free and open source \gls{XMPP} server implementations available:
\emph{Prosody} and \emph{ejabberd}.
This impression is confirmed by various comparisons on the web,
with ejabberd and prosody sharing the best coverage of implemented \glspl{XEP} and having the biggest market shares.
Technically,
they are equally good choices,
which is why the final decision was based on other aspects.
Before this thesis the IT Center was already looking into ejabberd as a replacement for Openfire,
regarding the \gls{OS} the IT Center was impartial and would accept the current release of any of the major Linux distributions.
ejabberd is a powerful \gls{XMPP} server implementation written in Erlang,
a programming language that is tailored to efficient clustering of services and has its own database included.
Being a full member of the Debian project since June
2009\footnote{\url{https://qa.debian.org/developer.php?login=debalance}}
and the maintainer of ejabberd in \emph{Debian GNU/Linux} since early
2014\footnote{\url{https://tracker.debian.org/pkg/ejabberd}}
(and thus indirectly for Ubuntu as well),
I already had a good understanding of ejabberd and detailed insights into the ejabberd packages in Debian.
Furthermore I was (and still am) in regular contact with some of the developers.
The current stable release of Debian (codename \emph{Jessie}) will have long term support until the end of April/May 2020 and it is quite easy to do an in-place upgrade to a newer release before then.
The latest ejabberd release before a major code refactoring (which will most likely introduce some regressions) is 16.09,
available for Debian Jessie from the official Debian Jessie Backports
repository\footnote{\url{https://backports.debian.org}}.
This is the same ejabberd release as currently running on \url{https://conversations.im},
home of the popular \gls{XMPP} client \emph{Conversations}.
Under these circumstances the choice was obvious:
ejabberd as the \gls{XMPP} server software and Debian GNU/Linux as the \gls{OS}.


\section{Setting up the XMPP server}
The IT Center created a \gls{VM} with Debian Jessie as the \gls{OS} on their serverhosting infrastructure,
tied it into their backup system and made it accessible via \gls{SSH}.
The new system was assigned the \gls{DNS} name \emph{jabber-dev.itc.rwth-aachen.de}.

In order to have every step of the setup reproducibly documented,
the IT automation tool \emph{Ansible} was used for setting up the new system by writing an Ansible \emph{playbook}.
Ansible itself is outside the scope of this thesis,
but there is some information about it included in \cref{apx:ansible}.
Basically,
Ansible remotely executes a series of commands on one or more hosts as defined in the playbook.
Ansible playbooks are plain text files written in YAML syntax and are fairly easy to read,
even for people who have never used Ansible or YAML before.
By not changing anything manually on the jabber-dev \gls{VM} it was ensured that the Ansible playbook would be a complete documentation of the setup process.
As a bonus,
the setup process can be automatically re-run at any time to verify that the system is in the desired state.

Now that a base system and a deployment process were in place,
the next step was to install and configure the chosen \gls{XMPP} server software \emph{ejabberd}.
The installation of ejabberd 16.09 on Debian Jessie is straight forward:
enable the aforementioned Jessie Backports repository and install the ejabberd package.
The only modification was to add a contributed module for push notifications (\gls{XEP}-0357) that had not yet been included in the official release.
Configuring ejabberd to correctly enable all the \glspl{XEP} in the current \gls{XMPP} Compliance Suite is a considerable task,
but the online documentation of ejabberd~\cite{configuring_ejabberd} is quite extensive and includes helpful examples.
To improve security and to get a better score on xmpp.net,
a file with custom 4096 bit long Diffie-Hellman parameters was generated through OpenSSL and ejabberd was configured to use it.
The final configuration file for ejabberd was 377 lines long and was reviewed by ejabberd developer Holger Weiß,
who found no fault in it.
The complete Ansible playbook (minus confidential data like passwords) is published in a git repository on
github.com\footnote{\url{https://github.com/debalance/ansible-playbook-jabber.rwth-aachen.de}}.\label{ansible-playbook}


\section{Migrating the user base}
\label{sec:migrate}
Replacing jabber.rwth-aachen.de should happen as smoothly as possible for the users,
the best case scenario would be them not noticing the change at all.
This means that all user account related data needs to be migrated from the old to the new server.
On the old server,
Openfire was storing all its data in a MySQL database.
Although there is \gls{XEP}-0227 which defines a format for exporting and importing user data between different servers,
Openfire 3.9.1 did not support that and all available Openfire plugins for exporting data failed to work as well.
The plugins were either incompatible with the version of Openfire or could not handle erroneous data,
e.\,g.\ \glspl{JID} with spaces.
Consequently,
the export had to be done manually directly from the MySQL database.
The exported data had to be processed and converted in a way that could be imported into ejabberd.

The export was done with a series of \gls{SQL} queries from a shell script,
resulting in text files containing the desired content where the different columns were separated with tabs.
This data could then be copied to a different computer and be processed there.

The conversion of the exported data was done with a series of self-written Python~3 scripts.
These scripts read the raw export data,
process it one line at a time and generate shell scripts that contain commands for importing the data to ejabberd via its commandline tool \emph{ejabberdctl}.
Python~3 proved to be a good choice as it automatically took care of all encoding issues in the raw data.
Its built-in data types were perfect for splitting the tab-separated lines into lists and continuing from there.
While at it,
the IT Center asked for accounts with invalid e-mail addresses to be excluded from the migration.
For that purpose the IT Center was sent a list of all registered e-mail addresses and returned a list with the invalid ones removed.
That list was then used to filter out all user data from accounts with invalid e-mail addresses.
The filtered data was stored separately so it could be imported later if necessary.

The resulting shell scripts could be copied onto the new system and executed there to do the actual import into ejabberd.
Using ejabberdctl for importing data into ejabberd is not the fastest
possibility\footnote{There is also an \acrshort{XML}-\acrshort{RPC} interface and a \gls{HTTP}(S) \acrshort{ReST} \acrshort{API}, both are considerably faster than ejabberdctl.},
but it is a very sturdy way to do it in terms of character encoding and string formatting.
The complete set of migration scripts is published on
github.com\footnote{\url{https://github.com/debalance/openfire2ejabberd}}.

With the user data imported,
the new \gls{XMPP} server was then successfully tested with several different clients and accounts.

\paragraph{A note about passwords:}
Openfire used to store the users' passwords in plaintext in the MySQL database.
In version 2.6.0 Openfire began to store new or updated passwords symmetrically encrypted;
therefore,
some passwords were stored in plaintext and others were stored encrypted.
The key for the symmetrical encryption was stored in the database as well.
There is a Python~2 script available on the Openfire
forums\footnote{\url{https://community.igniterealtime.org/thread/50526}}
to decrypt the passwords with that key.
While this is a serious security concern and a problem in terms of data privacy,
this was very helpful for the migration process as it made it possible for the users to keep their passwords.

ejabberd only stores SCRAM hashes of passwords in its database,
making it impossible to reconstruct the password later.
SCRAM is short for Salted Challenge Response Authentication Mechanism and is standardized by the \gls{IETF} in RFC~5802~\cite{rfc:5802}.


\section{Creating a new website}
\label{sec:website}
Now that the new system had a populated \gls{XMPP} server in operation it needed a website.
Several choices were presented to the IT Center at a meeting:
\begin{enumerate}[itemsep=0pt]
 \item port the old website to the new system, consisting of plain PHP + HTML for the static pages and a WordPress installation for the news feed
 \item set up a new WordPress installation for everything and manually add the functionality to register and manage Jabber accounts, for example with the plugin \emph{Ejabberd Account Tools}\footnote{\url{https://wordpress.org/plugins/ejabberd-account-tools}}
 \item adopt the Django-based homepage of jabber.at which is published as \gls{FOSS} on github.com\footnote{\url{https://github.com/jabber-at/hp}}
 \item suggest a completely different way
\end{enumerate}
Together it was decided that the Django-based approach was the most promising and best-looking one.
It was also the only one that had native support for multiple languages,
with the native English texts having already been translated to German.
Furthermore it would be a good base to later implement an extension for the secondary goal of managing shared roster groups.

\paragraph{Django}
is a powerful Python-based web framework originally developed by the Lawrence Journal-World newspaper in Lawrence,
Kansas, USA~\cite{docs.djangoproject.com}.
Like Python,
it has become very popular and is very well documented.
Still,
setting up a Django installation for production use is a complex task and requires at least a basic understanding of Django.
There is a very helpful tutorial for getting started available on the Django project's
website\footnote{\url{https://docs.djangoproject.com}}.

\pagebreak

\paragraph{The Django-based homepage setup}
consists of several different software components.
These are:
\begin{itemize}[itemsep=0pt]
 \item Apache~2 as the front-end web server, dispatching everything homepage related to uWSGI, except for static files which Django collects in a special folder for Apache~2 to serve directly (mostly css, javascript, and image files) 
 \item uWSGI, a system daemon that provides an interface between Apache~2 and (in this case) the Django framework, it basically runs a container with the Django instance in it
 \item MySQL~5, a relational database system that provides the database where Django stores its model/object related data
 \item Celery, an asynchronous task queue used for jobs that must be executed periodically and/or independently from user interaction (e.\,g.\ jobs that might take a rather long time should be executed in the background, like sending e-mails and downloading GPG keys from a keyserver)
 \item Redis, a non-volatile key-value database server used as an in-memory cache by Django to accelerate the execution of certain tasks
 \item Postfix, an \gls{MTA} responsible for sending out e-mails
 \item ejabberd as the \gls{XMPP} back-end, accessed via its \acrshort{XML}-\acrshort{RPC} interface or its \acrshort{HTTP}(S) \acrshort{ReST} \acrshort{API}
\end{itemize}
To get a better idea about the interactions between the different components involved confer the schematic in \cref{fig:django}.
The detailed setup process can be found in the published Ansible playbook (see \cpageref{ansible-playbook}).
Of course,
the new website was extensively tested and several issues were reported to the developer on github.com who solved them within a few days.
For example,
the server should only be usable by members of the Aachen universities,
but the website had no feature for limiting the allowed e-mail addresses to certain patterns to verify the affiliation.
To solve this problem,
a whitelist filter for e-mail addresses using regular expressions was
added\footnote{\url{https://github.com/jabber-at/hp/issues/8}}.
This whitelist filter could then be configured to only permit e-mail addresses ending in \emph{rwth-aachen.de},
\emph{fh-aachen.de}, etc.


\begin{figure}[htbp]
 \centering
 \includegraphics[width=1\textwidth]{drawings/system}
 \caption{Schematic of the Django-based homepage setup}
 \label{fig:django}
\end{figure}

\paragraph{Customization:}
\label{par:customizing}
The website has a central configuration file that allows configuring everything necessary to get it operational.
A visitor is then presented with an empty default installation.
On one side,
menu items,
pages,
and blogposts can be easily configured in the Django admin back-end.
On the other side,
changing the static sidebar or certain hardcoded strings and their translations requires changing the source code of the homepage and re-compiling the translations.
As the homepage software is published on github.com,
a fork was created and the changes made there\footnote{\url{https://github.com/debalance/hp}}.
The forked github repository was then used for the installation on the jabber-dev \gls{VM}.


\section{Collecting and processing statistics}
\label{sec:munin}
The old website was periodically collecting a few values from the \gls{XMPP} server and displayed them in a graphical format using the \gls{FOSS} \emph{RRDtool}.
The new website should do the same,
but instead of implementing it manually in the website the \gls{FOSS} \emph{Munin} was chosen.
Munin also uses RRDtool for storing and displaying the collected data,
but it has a flexible plugin architecture and ships with a large amount of standard plugins covering most default use cases.
The data is collected and processed every five minutes.
It is then stored and made available on the website via a \gls{CGI} run by the Apache~2 web server on demand as shown in
\cref{fig:django}.
By writing custom plugins it allows collecting data from arbitrary sources,
yet processing and displaying them in a standardized way.
The developer of the Django website has also developed several Munin plugins specifically for ejabberd and shares them as \gls{FOSS} on github.com\footnote{\url{https://github.com/fsinf/munin}}.
These plugins where slightly altered and added to the Ansible playbook for deployment.
Screenshots of some of the resulting graphs are included in \cref{chap:evaluation},
see \crefrange{fig:stats-activeusers}{fig:stats-connectiontypes}.


\section{Updating the GTC}
The existing \gls{GTC} were already extremely outdated regarding the old system and thus completely unsuitable for the new system.
Also,
they were only available in German,
whereas the new homepage supported English and German to cater for the increasing internationalization of the RWTH Aachen University.

The old German \gls{GTC} were updated and the IT Center's \gls{GTC} concerning their infrastructure were tied into it.
The new \gls{GTC} were then translated to English and the result of both versions was reviewed by the IT Center.
Once the IT Center had approved the new \gls{GTC} they were published on both the homepages of the old and the new system.
According to the old and the new \gls{GTC},
the new ones must be announced via E-Mail and Jabber at least 14 days prior to coming into effect
so users have enough time to delete their account if they do not agree.

The necessary mass e-mailing (jabber.rwth-aachen.de had approximately 4,600 registered accounts) was announced to the IT Center
so they could temporarily lift their anti-spam restrictions for jabber.rwth-aachen.de.
It was then carried out by a shell script that looped over all e-mail addresses in the Openfire database.

Previously,
the broadcast via Jabber would have been done through the Helga bot,
but since Helga was not available anymore a new solution was necessary.
The solution was to install and configure the Broadcast plugin for Openfire.
There was a small drawback to that plugin:
it did not allow to send the broadcast from the server \gls{JID},
meaning that everybody received the broadcast from the \gls{JID} \emph{philipp.huebner@jabber.rwth-aachen.de} instead of \emph{jabber.rwth-aachen.de}.
Since the notification via Jabber was not specified in detail in the \gls{GTC} this was considered irrelevant.
However,
it gave the users the opportunity to answer with questions and/or feedback.
Several users did so,
only a few deleted their accounts.
Two weeks later everything was ready for the actual migration.


\section{Going live}
During the implementation phase,
the new system had the \gls{DNS} name jabber-dev.itc.rwth-aachen.de while the old system continued using jabber.rwth-aachen.de.
Since the old server had already both a \gls{TLS} and a StartTLS port enabled on the default port numbers,
a \gls{SRV} record for \gls{XEP}-0368 (\emph{\gls{TLS} on connect}) could be added for jabber.rwth-aachen.de long before the migration.

The migration was scheduled and announced for early on a Friday morning.
December 16th,
2016 at around 6~AM Openfire was stopped and the user data migrated as described in \cref{sec:migrate}.
The homepage on the old system was configured to redirect everything to its news feed where a scheduled blogpost was automatically published at 6~AM,
stating that the migration to the new system was now in progress.

The import on the new server consisted of about 62,000 ejabberdctl commands and it took about two hours and 15 minutes to execute them.
This amount of downtime was known beforehand and had been deemed appropriate for the migration by the IT Center.
When the import was done,
the user accounts had to be imported into the Django-based website as well.
Using Django's interactive python shell and the \acrshort{XML}-\acrshort{RPC} interface to ejabberd,
that task was completed within a few more minutes.

After a few checks to see if everything was working,
the IT Center changed the \gls{DNS} entry jabber.rwth-aachen.de so that it now resolved to the \gls{IP} address of the new server.
The \gls{TTL} for that entry had been set to one minute a few days earlier so that every \gls{DNS} server would pick up the change almost instantly.
The change was made around 9~AM,
thereby ending the downtime and finishing the migration after three hours.

A minute after the change had become effective there were already 70 clients connected to the new server,
five minutes later it was about 150.
Later that day the count rose to the same values as with the old system the days before.
A few people contacted the Jabber team because their accounts had stopped working.
In all cases it was due to an invalid e-mail address.
Once the account had been re-created with the correct e-mail address
the according user data was imported and the affected user account was working again.

\clearpage

\section{Shared Roster Groups}
\gls{XMPP} itself does not have a concept of groups~\cite{ieee:xmpp_dynamic_groups}.
It is possible to group contacts in one's contact list,
but these groupings are visible only to that particular user.
Many \gls{XMPP} servers (including ejabberd) allow to configure groups server-side and then automatically modify the users' rosters accordingly,
but this feature is available to server administrators only and is not part of the \gls{XMPP} standard
(which simply does not cover this at all).
Obviously,
giving every user administration privileges is out of the question,
so it was necessary to implement a solution with more fine-grained control,
realized by extending the Django-based website.

A Django project consists of one or more applications,
called \emph{apps}.
In case of this website the apps are \emph{account},
\emph{blog},
\emph{bootstrap},
\emph{core},
and \emph{feed},
brought together in the project \emph{hp}.
For the purpose of managing shared roster groups a new Django app called \emph{groups} was added and implemented from scratch.

First,
the shared roster group and its relations to other objects had to be modeled.
This is done in the file \emph{models.py} via Python classes.
The group model was simply called \emph{Group}.
The groups in ejabberd have names,
descriptions,
members,
and a display field.
The display field defines what will be shown to the group members in their roster.
With only a few exceptions this field is usually equal to the group name.
Obviously,
groups in Django must have at least the same fields as in ejabberd,
but they can have additional ones.
In this case another field called \emph{owners} was added to the \emph{Group} model.
Just like the \emph{members} field it is a many-to-many field to the \emph{User} model from the \emph{account} app.
This means a group can have several members and several owners,
just like a user can be owner and/or member of several different groups.
While the members field is going to be synchronized with ejabberd,
the owners field is internal to Django and will be used to decide if somebody is authorized to manage a certain group.
For the many-to-many fields \emph{members} and \emph{owners},
the models \emph{membership} and \emph{ownership} had to be modeled as well.
The complete \emph{models.py} is available in the published git repository (see \cref{par:customizing}).
Once the modeling process is finished,
Django takes care of all \acrshort{SQL}-related tasks automatically,
so in most cases the developer never interacts with the database directly.

Second,
the file \emph{urls.py} had to be populated.
This file maps certain \glspl{URL} to Django functions which are called \emph{Views}.
\glspl{URL} can also contain named or unnamed arguments.

Third,
the views had to be written,
which constituted the biggest part of the development work.
This is done in the file \emph{view.py},
where each view is either a function or a class.
The web pages shown to the user are generated from templates that had to be written as well and are located in the templates folder.
There is one template per view,
the content is written in English and translated to German with the help of Django's localization tools.
The templates are rendered and shown to the user through the different views.
For the user to submit data,
forms are needed and used.
This is done in the file \emph{forms.py} via Python classes.
The forms are added to the templates and the user's input is validated upon transmission of the completed form.
The view responsible for that form then acts accordingly and returns another (or the same) rendered template.
The groups app has four main views:
an overview,
a list of the user's memberships,
a list of the groups the user owns and a form to create a new group.
Through those pages the user can access additional views for showing,
editing,
leaving,
or deleting a group.
Several examples can be seen in \crefrange{fig:groups-overview}{fig:groups-edit}.

\begin{figure}[htbp]
 \centering
 \frame{\includegraphics[width=1\textwidth]{screenshots/groups_overview}}
 \caption{Django user view \emph{groups:overview}}
 \label{fig:groups-overview}
\end{figure}

\begin{figure}[htbp]
 \centering
 \frame{\includegraphics[width=1\textwidth]{screenshots/groups_memberships}}
 \caption{Django user view \emph{groups:membership}}
 \label{fig:groups-memberships}
\end{figure}

\begin{figure}[htbp]
 \centering
 \frame{\includegraphics[width=1\textwidth]{screenshots/groups_edit}}
 \caption{Django user view \emph{groups:edit}}
 \label{fig:groups-edit}
\end{figure}

When all this was done the website on the development server had a working groups app.
It worked well,
but did not interact with ejabberd yet.
For that the Django \gls{XMPP} interface had to be extended.
Since by then the \acrshort{XML}-\acrshort{RPC} interface was considered deprecated by the ejabberd developers in favor of the new
\gls{HTTP}(S) \acrshort{ReST} \gls{API},
the website was switched over accordingly.
This was only a matter of configuration as the Django \gls{XMPP} back-end already supported both \glspl{API}~\cite{xmpp-backends}.
However,
the Django \gls{XMPP} back-end had to be extended as it did not expose the needed functions for managing shared roster
groups~\cite{xmpp-backends, ejabberd_api_reference}.
Thanks to Python's inheritance features,
a new Python class could simply be derived from the existing back-end and the functions for managing shared roster groups were added.
These commands where then added into the \emph{save()} and \emph{delete()} commands of the models.
From then on all changes to Django groups were forwarded to ejabberd automatically.

Last but not least,
the Django admin interface had to be customized.
One of Django's great features is the automatic creation of an admin back-end for registered models.
The default interface however does not always completely suit ones needs and therefore can be customized.
This is done in the file \emph{admin.py}.
For example,
the group name was made read-only because ejabberd has no command to rename a group.
Furthermore it was defined what fields should be displayed where and how and which ones should be used for search.
Finally,
a synchronization feature between ejabberd and Django was added for each direction,
either for all groups or selected ones only.
See figs.~\ref{fig:groups_admin_list} and~\ref{fig:groups_admin_change} for examples.
The groups app now consisted of almost 2,000 lines of code and text.
It is published in the aforementioned git repository on
github.com\footnote{\url{https://github.com/debalance/hp}}.

\begin{figure}[htbp]
 \centering
 \frame{\includegraphics[width=1\textwidth]{screenshots/groups_admin_list}}
 \caption{Django admin view \emph{list}}
 \label{fig:groups_admin_list}
\end{figure}

\begin{figure}[htbp]
 \centering
 \frame{\includegraphics[width=1\textwidth]{screenshots/groups_admin_change}}
 \caption{Django admin view \emph{change}}
 \label{fig:groups_admin_change}
\end{figure}

Now that the groups app was finished and successfully tested on the development system,
it had to be deployed on the production system.
This happened during a scheduled and announced maintenance window early on a Friday morning.
February 3rd, 2017 the new app was deployed via Ansible just like everything else before,
then a full synchronization of all groups from ejabberd to Django was executed.
Afterwards,
a few people that had already asked for certain groups were added as owners to those groups.
A blogpost was added for everyone else,
stating that ownerless groups could be claimed by any group member by simply contacting the Jabber team.
Furthermore,
all users were sent a Jabber message announcing the new groups app via server broadcast,
a feature that is natively supported by ejabberd.
Within an hour several new groups had been created and were being populated by users.

