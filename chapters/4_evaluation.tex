\chapter{Evaluation}
\label{chap:evaluation}
The following paragraphs evaluate the new system behind jabber.rwth-aachen.de in four different aspects:
compliance,
security,
user satisfaction,
and sustainability.
While compliance and security can be tested with dedicated tools,
user satisfaction is a rather subjective matter and for sustainability only an educated guess is possible.

\paragraph{Regarding the compliance of \gls{XMPP} servers,}
the \emph{Compliance Tester for \gls{XMPP} Servers} by Daniel Gultsch is a great tool for collecting comparable results.
It is also the only tool freely available.
The compliance tester checks a given server for compatibility with the compliance levels defined in \gls{XEP}-0387
(\emph{XMPP Compliance Suites 2017}),
which was introduced in \cref{xep-0387}.
The compliance tester requires valid credentials to an account on the server in question in order to conduct its tests.
According to its output,
jabber.rwth-aachen.de is compliant with \emph{Advanced Server Core},
\emph{Advanced Server \gls{IM}},
and \emph{Advanced Server Mobile}.
Since jabber.rwth-aachen.de has support for \gls{BOSH} enabled in ejabberd,
it should also be compliant with \emph{Advanced Server Web},
but that is not checked by the compliance tester.
The results can be submitted to Daniel Gultsch on
github.com\footnote{\url{https://github.com/iNPUTmice/ComplianceTester}}
and are then published on his website\footnote{\url{https://gultsch.de/compliance\_ranked.html}},
where \mbox{jabber.rwth-aachen.de} currently shares the first place with relatively few other servers,
as it is 100\% compliant.
Table~\ref{tab:xmpp-compliance} compares the detailed test results for the old and the new system of jabber.rwth-aachen.de.

\begin{table}[htbp]
 \centering
 \begin{tabular}{lcccc}
  \toprule
  \addlinespace XMPP Server				& & Openfire 3.9.1	& & ejabberd 16.09	\\
  \addlinespace Date of test				& & October 1st 2016	& & April 1st 2017	\\
  \midrule
  \addlinespace XEP-0045: Multi-User Chat		& & \passed		& & \passed		\\
  \addlinespace XEP-0065: SOCKS5 Bytestreams (Proxy)	& & \passed		& & \passed		\\
  \addlinespace XEP-0115: Entity Capabilities		& & \failed		& & \passed		\\
  \addlinespace XEP-0163: Personal Eventing Protocol	& & \passed		& & \passed		\\
  \addlinespace XEP-0191: Blocking Command		& & \failed		& & \passed		\\
  \addlinespace XEP-0198: Stream Management		& & \failed		& & \passed		\\
  \addlinespace XEP-0237: Roster Versioning 		& & \failed		& & \passed		\\
  \addlinespace XEP-0280: Message Carbons		& & \failed		& & \passed		\\
  \addlinespace XEP-0313: Message Archive Management	& & \failed		& & \passed		\\
  \addlinespace XEP-0352: Client State Indication	& & \failed		& & \passed		\\
  \addlinespace XEP-0357: Push Notifications		& & \failed		& & \passed		\\
  \addlinespace XEP-0363: HTTP File Upload		& & \failed		& & \passed		\\
  \addlinespace XEP-0368: SRV records for XMPP over TLS	& & \failed		& & \passed		\\
  \addlinespace
  \bottomrule
 \end{tabular}
 \captionsetup{width=0.65\textwidth}
 \caption{Comparison of results from the \emph{Compliance Tester for XMPP Servers}}
 \label{tab:xmpp-compliance}
\end{table}


\paragraph{Regarding the security of \gls{XMPP} servers,}
the \emph{\gls{IM} Observatory} at \href{https://xmpp.net}{xmpp.net} is the best measure.
It offers a detailed outside analysis of all security related aspects for a given \gls{XMPP} domain,
for both \gls{s2s} and \gls{c2s} connections.
While security features are hard facts,
there is some room for interpretations when it comes to connectivity.
A perfect score means that only servers and clients supporting the latest security standards can exchange messages with the tested server,
locking out almost all but the most current releases of all operating systems.
A rather low score means that servers and clients with more sophisticated security requirements will refuse to exchange messages with the server in question because it is not sufficiently secure.

The reports from xmpp.net are divided into four different scoring criteria:
certificate trust,
key exchange,
encryption protocol,
and cipher suite.
Every criteria gets a score between 0 and 100.
From these scores together every tested server is given a grade from 'A' to 'F',
with 'A' being the best one.

The certificate score is either 0 (in which case the grade is capped to 'F') for untrusted or invalid certificates,
or 100.
With the certificate issued by the IT Center's \gls{CA},
jabber.rwth-aachen.de is trusted and gets a certificate score of 100 every time.

The key exchange score depends on the size of the public key and whether any cipher suites are enabled that do not use that key.
To get a key exchange score of 100,
a key size of at least 4096 bits is necessary and all enabled cipher suites must use it.
The certificate issued by the IT Center's \gls{CA} is 2048 bits,
limiting the key exchange score for jabber.rwth-aachen.de to 90.

The protocol score is the average of the least and the most secure protocol supported by the server.
To get a protocol score of 100,
the server must only support \gls{TLS} 1.2,
which would for example hinder all mobile devices with Android < 4.4 from
connecting\footnote{\url{https://www.ssllabs.com/ssltest/clients.html}}.
Since \gls{TLS} >= 1.0 is still considered secure,
jabber.rwth-aachen.de allows \gls{TLS} in versions 1.0,
1.1 and 1.2,
scoring 95 points in this category.
Unencrypted connections are still allowed in order to not loose connectivity with the \gls{XMPP} servers from Google.
Although discouraged due to lack of encryption,
it is still possible to add gmail contacts to one's \gls{XMPP} roster and chat with them,
even if they are using Google Hangouts instead of a full-featured \gls{XMPP} client.

The cipher score is the average of the cipher suite with the smallest key and the cipher suite with the largest key.
To get a score of 100 here,
no cipher suites with key sizes lower than 256 bits must be supported,
but even xmpp.net itself recommends to keep support for 128 bit \acrshort{AES} for compatibility reasons.

Taking all the aforementioned explanations into account,
the goal is not to score 100 points in every category but to get the overall grade 'A'.
The scoring system is explained in more detail on the \gls{IM} Observatory's homepage\footnote{\url{https://xmpp.net/about.php}}.
Because the client and server reports for jabber.rwth-aachen.de were more or less identical in every case,
only the server reports are shown on the following pages,
the client reports can be accessed at the given \glspl{URL}.

As mentioned earlier,
the old system before this thesis only received a 'C',
as shown in \cref{fig:xmpp.net-old}.
After the improvements on \cpageref{chap:implementation} it received a 'B',
compare with \cref{fig:xmpp.net-intermediate} to see the changes.
The new system at the end of this thesis received an 'A',
as can be seen in \cref{fig:xmpp.net-new}.
Anybody can re-run the test on xmpp.net at any time to get current results.

\begin{figure}[htbp]
 \centering
 \rule{1\textwidth}{0.4pt}\\
 \vspace{0.5cm}
 \includegraphics[width=1\textwidth]{screenshots/xmppnet_old}
 \rule{1\textwidth}{0.4pt}
 \captionsetup{width=0.75\textwidth}
 \caption[xmpp.net test results for Openfire 3.9.1]{
  xmpp.net test results for the old system\\
  \gls{c2s}: \url{https://xmpp.net/result.php?id=477600}\\
  \gls{s2s}: \url{https://xmpp.net/result.php?id=477601}}
 \label{fig:xmpp.net-old}
\end{figure}

\begin{figure}[htbp]
 \centering
 \rule{1\textwidth}{0.4pt}\\
 \vspace{0.5cm}
 \includegraphics[width=1\textwidth]{screenshots/xmppnet_intermediate}
 \rule{1\textwidth}{0.4pt}
 \captionsetup{width=0.75\textwidth}
 \caption[xmpp.net test results for Openfire 3.9.3]{
  xmpp.net test results for the intermediate system\\
  \gls{c2s}: \url{https://xmpp.net/result.php?id=589022}\\
  \gls{s2s}: \url{https://xmpp.net/result.php?id=561488}}
 \label{fig:xmpp.net-intermediate}
\end{figure}

\begin{figure}[htbp]
 \centering
 \rule{1\textwidth}{0.4pt}\\
 \vspace{0.5cm}
 \includegraphics[width=1\textwidth]{screenshots/xmppnet_new}
 \rule{1\textwidth}{0.4pt}
 \captionsetup{width=0.75\textwidth}
 \caption[xmpp.net test results for ejabberd 16.09]{
  xmpp.net test results for the new system\\
  \gls{c2s}: \url{https://xmpp.net/result.php?id=615564}\\
  \gls{s2s}: \url{https://xmpp.net/result.php?id=615565}}
 \label{fig:xmpp.net-new}
\end{figure}

\clearpage

\paragraph{Regarding the user satisfaction,}
the only quantifiable sources of information available are the statistics and log entries collected on the new system itself.
Only few users ever contact the Jabber team,
either through the contact form on the website,
directly via e-mail,
Jabber,
or personally.
The little feedback that has been received so far was completely positive though.

The statistics are freely available in graphical form on the website of jabber.rwth-aachen.de as described in \cref{sec:munin},
but for security reasons that part of the website is only accessible from within the RWTH Aachen University's network.
The relevant figures collected there are the numbers of registered users,
connected users,
active users per week and active users per month.
Within the first three months after the new system became available,
57 new accounts have been registered and none have been deleted.
There are about 380 active users per week (see \cref{fig:stats-activeusers}) and 480 per month.
The daily maximum of simultaneously active client connections is approximately 250 (see \cref{fig:stats-connections}).
One reason why this number is much lower than the number of active users per week is because clients on mobile devices
usually do not keep a persistent connection while the screen is turned off,
instead they rely on \gls{XEP}-0357 (\emph{Push Notifications}).
They should only (re-)connect when the screen is turned on or a push notification has been received.
Furthermore,
users active in a given week can be online at different times.
Since \gls{TLS} is enforced for \gls{c2s} connections and \gls{IP}v6 is not available yet,
all client connections are \gls{TLS} encrypted and transported via \gls{IP}v4 (see \cref{fig:stats-connectiontypes}).

The old system only collected a few rudimentary statistics.
In the months before this thesis,
the number of simultaneously online users varied between a minimum of 80 and a maximum of 200,
which is 20\,-\,30\% less compared to the new system when this thesis was finished.
Given that the old system did not support push notifications,
meaning that online users had to stay connected the whole time,
the difference might be even bigger.

\begin{figure}[htbp]
 \centering
 \includegraphics[width=1\textwidth]{screenshots/stats_active_users}
 \caption{Graph of active XMPP users per week}
 \label{fig:stats-activeusers}
\end{figure}

\begin{figure}[htbp]
 \centering
 \includegraphics[width=1\textwidth]{screenshots/stats_connections}
 \caption{Graph of XMPP connections}
 \label{fig:stats-connections}
\end{figure}

\begin{figure}[htbp]
 \centering
 \includegraphics[width=1\textwidth]{screenshots/stats_connection_types}
 \caption{Graph of XMPP client connection types}
 \label{fig:stats-connectiontypes}
\end{figure}

The system's log entries are only accessible for members of the Jabber team.
They confirm the number of new accounts and describe the usage of the new groups app within the first week after it became availabe:
four new groups have been created,
one group has been deleted and ten groups were changed in at least one way during the first week,
accumulating 82 changes altogether.
Since then,
the daily numbers of changes to groups have fallen to near zero.

\paragraph{Regarding the sustainability,}
only an estimation can be given.
The developers of ejabberd pride themselves with ''massive scalability'',
claiming to have managed handling two million active users with only one big \gls{VM} (an Amazon EC2
instance)\footnote{\url{https://blog.process-one.net/ejabberd-massive-scalability-1node-2-million-concurrent-users/}}.
For every online user ejabberd needs ''a few hundred kilobytes of memory'',
according to the developers.
The necessary CPU power depends heavily on how active the online users are and is extremely difficult to quantify.
Given that jabber.rwth-aachen.de is a \gls{VM} on the IT Center's serverhosting infrastructure
it can easily be given more memory and more CPU power when the need arises.
Disk space can just as easily be extended.

All load factors can be monitored comfortably through the graphical statistics,
made available on the website via Munin.
As can be seen in \cref{fig:stats-cpu},
the CPU is mostly idle.

\begin{figure}[htbp]
 \centering
 \includegraphics[width=1\textwidth]{screenshots/stats_cpu}
 \caption{Graph of XMPP server CPU load}
 \label{fig:stats-cpu}
\end{figure}

With 220 users connected,
the current system uses 965 megabytes of memory of the available two gigabytes.
Compared to the development system with zero users connected but an otherwise identical setup,
only ejabberd and MySQL are using different amounts of memory.
Dividing the difference by 220 users this results in 0.7 megabytes per user,
matching the claim of the ejabberd developers mentioned above.
Dividing the dynamically available memory by 0.7,
the system's memory would be completely allocated when the number of simultaneously online users reaches 1260,
which is over five times the current daily maximum of 250.

Using \gls{XEP}-0363 (\emph{\gls{HTTP} File Upload}) as introduced in \cref{xep-0363},
every user can upload up to 150 megabytes of files to jabber.rwth-aachen.de as that is the configured quota.
Two months after \gls{XEP}-0363 has become available on jabber.rwth-aachen.de,
the average quota used is 6.25 megabytes.
The \gls{VM} has a dedicated partition for these uploads of 100 gigabytes,
100,666 megabytes of usable space to be exact.
If every user were to completely use his or her quota,
the partition could serve up to 670 users before needing to be extended.
Given the current average,
the partition could just as well serve up to 16,000 users.
Note that uploaded files are automatically deleted after 31 days.
Users can also delete individual (or all) uploaded files themselves via the website.

Taking all aspects into account and extrapolating the slow increase of active users,
jabber.rwth-aachen.de's resources are sufficient for several more years.
In the end it all comes down to the users who decide in numbers and behavior if and when jabber.rwth-aachen.de is going to need more resources.

