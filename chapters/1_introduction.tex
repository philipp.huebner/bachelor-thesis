\chapter{Introduction}
\label{chap:introduction}
\gls{XMPP},
also known as Jabber,
is the internet standard for near real-time,
extensible instant messaging and presence information.
Originally designed for communication between human end
users\footnote{\url{https://xmpp.org/uses/instant-messaging}}
and adopted by popular modern messaging apps like WhatsApp,
GoogleTalk,
and Facebook Messenger,
it is nowadays also often used for communication with or between hardware devices.
A good example are \gls{VOIP} telephones where \gls{XMPP} provides the phone directory and presence
information\footnote{\url{http://www.cisco.com/c/en/us/td/docs/voice\_ip\_comm/cucm/srnd/8x/uc8x/presence.html}}.
Also,
messaging between phones and server allows for reliable transport of configuration instructions in both directions.
Another good example is the \gls{IoT},
where \gls{XMPP} can provide an already standardized,
safe,
and lightweight way for \acrshort{IP} capable devices to interact with each
other\footnote{\url{https://xmpp.org/uses/internet-of-things}}.
A rather new field of application for \gls{XMPP} is modern Smart Grid / Smart Market Communication,
where \gls{XMPP} was recently added to \acrshort{IEC}~61850-8~\cite{iec:61850.1, iec:61850.2}.

There is an ongoing public debate about \gls{IM} in terms of data privacy / data protection.
When proprietary \gls{IM} service providers are located in different countries than their users,
the users' data is regularly transported around the globe and they have no control over where it might be stored and who might have access to it.
Furthermore,
proprietary \gls{IM} service providers own all the servers in their closed network and consequently have the ability to collect all available metadata as well as the actual user data.
\gls{XMPP} as a whole stands for rather the opposite: an open,
federated,
and distributed system that allows anybody to set up a server and join in.
As long as one communicates only with users from the same server,
one's data never leaves that server and therefore cannot be intercepted by foreign server administrators.
It also ensures that if a single server fails or gets cut off from the internet,
the rest will continue to work undisturbed.
Since there is no central location in the network where a single service/host provider could siphon off all metadata,
that risk is significantly decreased as well.
For these reasons the RWTH Aachen University has had its own \gls{XMPP} server for over a decade,
established and maintained by a group of students since August 2006 and hosted at the IT Center since March 2007.
Unfortunately,
due to lack of volunteers from the student body,
the system has effectively not been maintained since 2014 and was handed over to the IT Center in 2016.

Up to now the system is running an \gls{XMPP} server software (Openfire) released in February
2014\footnote{\url{https://community.igniterealtime.org/blogs/ignite/2014/02/06/openfire-391-has-been-released}}
and is based on an operating system (CentOS 5) that was released on April 12th,
2007 and whose security support ends on March 31st,
2017\footnote{\url{https://wiki.centos.org/About/Product}}.
Connectivity with other \gls{XMPP} servers is seriously impaired because more and more servers require strong transport encryption and server identity verification which this system is not yet capable of because of its outdated software.
As a consequence,
users are complaining or wondering why they cannot chat with users from other servers anymore and are suffering from unreliable message delivery.
Furthermore,
the system has no specific support for mobile devices yet.
The list of problems grows and the number of active users declines as time progresses.
Consequently,
it is time for a change,
the process of which is the topic of this thesis.
The goals of this thesis are to:
\begin{itemize}
\item switch to a recent \gls{XMPP} server software that is up to date with current \gls{XMPP} technologies and an \gls{OS} that is supported for several more years
\item migrate all the user accounts and their associated data to ensure continuous usability and a smooth transition
\item give the website a major do-over both technically and in terms of content
\item update the \gls{GTC}, which requires contacting all existing users via e-mail and Jabber
\end{itemize}
In addition to these primary goals there is another topic which is the focus of a secondary goal:
In the past,
jabber.rwth-aachen.de was running a bot called Helga,
written by the students that were maintaining the server several years earlier.
At some point in time,
Helga stopped working and nobody was around anymore who knew how to get it back.
Helga had quite a list of features that could be used by simply chatting with it.
Most of these features are nowadays implemented by the \gls{XMPP} servers themselves or are part of the default software on smartphones.
Only one important feature remains: managing shared roster groups.
Through Helga,
everybody could create a shared roster group and manage it.
Every member of such a group would automatically have the whole group in his or her contact list.
There were about 40 of these groups when Helga disappeared,
suddenly burdening the IT Center with the task of maintaining these groups manually.

For these reasons,
the secondary goal of this thesis is to implement a solution that allows users to create and manage shared roster groups by themselves.
The solution does not need to be a bot,
but it should work no matter which client software is being used.

In the next chapter the basics of \gls{XMPP} and related internet technologies are explained before
the steps taken for achieving the goals outlined above are described in \cref{chap:implementation}.
In \cref{chap:evaluation} the results are evaluated and in the final \cref{chap:conclusion}
conclusions about the success of the entire project are drawn and an outlook for possible follow up work and future projects is given.
